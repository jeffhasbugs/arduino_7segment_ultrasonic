# arduino_7segment_ultrasonic
![Diagram of circuit](./screenshot.png)

This project displays distance measured by ultrasonic sensor on a 7-segment display. 

The display is driven with a shift register to "extend" the Arduino pinouts. One shift register sends
instructions on which segments in a digit to turn on to form a number, while the other tells the display 
which one of four digits the instructions apply to.

Each shift register is only capable of sending 8 bits at once, so we cannot afford to send all 4 digits
worth of information (4 * 7 = 28 bits) in one go. To circumvent that, we control the 4 digits 
individually and turn them on in sequence such that each digit flashes once every 60ms, fast enough that
the images persist in the retina and the eye cannot tell the diference.

### Note on how new bits are added on the shift register
When adding a new bit, the 74HC595 shift register places the new bit to the leftmost position and
"pushes" all bits to the right of the new bit downwards Q1 ... Q7 (pins used to output the bits),
discarding the rightmost bit.

Or in another way, Q1 lets you access the most significant bit (left-most bit or the most recently
added bit) regardless of the size of the data currently in the register.

## Behavior
Ultrasonic distance sensor returns time it takes for a sound wave to travel a certain distance,
bounce back, and return from the nearest object. This distance is calculated on the Arduino. If
the distance is less than 100cm, it is displayed on the 7-segment display and the buzzer and LED
beeps and flashes, respectively.

As the speed of sound used in the wokwi simulator seems to be 343m/s. To convert m/s to
cm/microsecond, the constant is multiplied by 100 and divided by 10^6.
```
distance = time in microseconds * (343 / 10^6 * 100)
```

## Simulation
This project can be simulated on wokwi: 
[https://wokwi.com/projects/337078102726279764](https://wokwi.com/projects/337078102726279764)

## References
wokwi-7segment Reference: https://docs.wokwi.com/parts/wokwi-7segment
millis() Reference: https://www.arduino.cc/reference/en/language/functions/time/millis/
